<?php
define('IGNORE_RECAPTCHA', FALSE);
define('SLIDER__MIN', 1);
define('SLIDER__MAX', 5000);
define('DEBUG', isset($_GET['debug']));
define('CAPTCHA_SECRET', '6LfKIj4aAAAAAN8f7Su77jLkUTymKqbVsYfRmxpk');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");


if (DEBUG)
	header('Content-type: text/html; charset=utf-8"');
else
	header("Content-type: application/json; charset=utf-8");

date_default_timezone_set('Europe/Moscow');

if (DEBUG) : ?>

	<!DOCTYPE html>
	<html>
	<head>
		<title>Chase X server preview</title>

		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<link rel="stylesheet" href="./highlight.min.css" />
		<link rel="stylesheet" href="./style.css" />
	</head>
	<body>
<?php endif;

include_once 'fns.php';

$r = $_REQUEST;

$f = file_get_contents('./data.json');
$o = json_decode(stripslashes($f));

$c = NULL;

$response = (object)[
	"status" => 404,
	"result" => NULL,
	"message" => "Я не знаю ответ на этот запрос ))."
];

$query = isset($r['query']) ? trim($r['query']) : NULL;
$access = !empty($query);

$authToken = isset($r['token']) ? $r['token'] : NULL;


$notRequireAuth = ['menu', 'page', 'sign-in', 'sign-up', 'data', 'generete-promocodes'];


if ($access) {

	$authUser = NULL;
	if (!in_array($query, $notRequireAuth)) {
		$authUser = getUserByToken($authToken, $o->users); 
		if (empty($authUser)) {
			$access = FALSE;
			$response->status = 401;
			$response->message = 'Неавторизованный пользователь';
		}
	}

	if ($access) {
		switch ($r['query']) {
			case 'menu': {
				$l = isset($r['location']) ? trim($r['location']) : FALSE;

				if (!$l) {
					$response->status = 200;
					$response->result = $o->menu;
					$response->message = "success";
				} else {
					if (isset($o->menu->$l)) {
						$response->status = 200;
						$response->message = "success";
						$response->result = $o->menu->$l;
					} else {
						$response->message = "Расположение меню не найдено. Пожалуйста, проверьте аргумент местоположения еще раз!";
					}
				}
				break;
			}
			case 'page': {
				$n = isset($r['name']) ? trim($r['name']) : 'default';

				if (isset($o->pages->$n) && file_exists("pages/$n.html")) {
					$page = $o->pages->$n;

					$response->status = 200;
					$response->message = 'success';
					$response->result = [
						"heading" => $page->heading,
						"content" => file_get_contents("pages/$n.html")
					];
				} else {
					$response->message = "Страница не найдена";
				}
				break;
			}
			case 'user': {
				if (!isset($r['token'])) {
					$response->status = 401;
					$response->message = 'Для продолжения пожалуйста авторизуйтесь на сайте';
				} else {
					if (!empty($authUser)) {
						$response->status = 200;
						$response->result = [
							"id" => $authUser->id,
							"color" => $authUser->color,
							"total" => $authUser->total,
							"username" => $authUser->username
						];
						$response->message = 'success';	
					} else {
						$response->status = 401;
						$response->message = 'Срок действия авторизации истекло, пожалуйста авторизуйтесь заново';
					}
				}
				break;
			}
			case 'sign-up': {

				$agree = trim($r['agree']);
				$username = trim($r['username']);
				$password = trim($r['password']);
				$rToken = trim($r['recaptchaToken']);

				if (usernameExists($username, $o->users)) {
					$response->status = 403;
					$response->message = "Имя пользователя уже занято";
				} else if ((empty($rToken) || !recaptcha($rToken)) && !IGNORE_RECAPTCHA) {
					$response->status = 400;
					$response->message = "Пожалуйста подтвердите что вы человек";
				} else {
					$token = bin2hex(random_bytes(64));

					$user = createUser($username, $password);
					$user->token = $token;

					$o->users[] = $user;

					$response->status = 200;
					$response->result = [
						"user" => [
							"id" => $user->id,
							"color" => $user->color,
							"total" => $user->total,
							"username" => $user->username
						],
						"token" => $user->token
					];

					$c = $o;
				}
				break;
			}
			case 'sign-in': {
				if (!empty($o->users)) {
					$find = NULL;
					$user_index = NULL;
					$u = isset($r['username']) ? trim($r['username']) : NULL;
					$p = isset($r['password']) ? trim($r['password']) : NULL;
					$rToken = isset($r['recaptchaToken']) ? trim($r['recaptchaToken']) : NULL;

					if (empty($u) || empty($p)) {
						$response->status = 400;
						$response->message = "Пожалуйста заполните  все поля";
					} else if ((empty($rToken) || !recaptcha($rToken)) && !IGNORE_RECAPTCHA) {
						$response->status = 400;
						$response->message = "Пожалуйста подтвердите что вы человек";
					} else {
						foreach ($o->users as $index => $user) {
							if ($user->username === $u && $user->password === md5($p)) {
								$user->index = $index;
								$find = $user;
								break;
							}
						}

						if (!empty($find)) {
							$token = bin2hex(random_bytes(64));

							$response->status = 200;
							$response->result = [
								"user" => [
									"id" => $find->id,
									"color" => $find->color,
									"total" => $find->total,
									"username" => $find->username
								],
								"token" => $token,
							];
							$response->message = "success";

							$o->users[$find->index]->token = $token;

							$c = $o;
						} else {
							$response->message = "Неверный имя пользователя или пароль";
						}
					}
				}
				break;
			}
			case 'edit-account': {
				$password = isset($r['password']) ? trim($r['password']) : '';

				$uppercase = preg_match('@[A-Z]@', $password);
				$lowercase = preg_match('@[a-z]@', $password);
				$number    = preg_match('@[0-9]@', $password);

				if($uppercase && $lowercase && $number && strlen($password) >= 8) {
					$token = bin2hex(random_bytes(64));

					$o->users[$authUser->index]->password = md5($password);
					$o->users[$authUser->index]->token = $token;

					$response->status = 200;
					$response->result = [
						"user" => [
							"id" => $authUser->id,
							"color" => $authUser->color,
							"total" => $authUser->total,
							"username" => $authUser->username
						],
						"token" => $token
					];
					$response->message = "success";

					$c = $o;
				} else {
					$response->status = 401;
					$response->message = "Пароль должен быть не менее 8 символов, только латинские буквы, содержать цифра и прописная буква";
				}
				break;
			}
			case 'promo-code': {
				$code = getPromoCode($r['code'], $o->promocodes);
				if (!empty($code)) {
					$o->users[$authUser->index]->total += $code->total;
					array_splice($o->promocodes, $code->index, 1);

					$response->status = 200;
					$response->result = $code;

					$c = $o;
				} else {
					$response->message = "Указан недействующий промокод";
				}
				break;
			}
			case 'create-ticket': {
				$message = isset($r['message']) ? trim($r['message']) : '';
				$subject = isset($r['subject']) ? trim($r['subject']) : '';
				$section = isset($r['section']) ? trim($r['section']) : 'other';

				if (!empty($subject)) {
					$ticket = createTicket($authUser, $section, $subject, $message);

					$o->support[] = $ticket;

					$c = $o;

					$response->status = 200;
					$response->result = $ticket;
					$response->message = 'success';
				} else {
					$response->status = 401;
					$response->message = 'Пожалуйста укажите тему тикета';
				}
				break;
			}
			case 'get-tickets': {

				if (!empty($authUser->id)) {
					$tickets = getTickets($authUser->id, $o->support);
					
					$sanitializTickets = [];
					foreach ($tickets as $index => $ticket)
						$sanitializTickets[] = getSanitializTicket($ticket, $o->sections, $o->supportStatuses);

					$response->status = 200;
					$response->result = $sanitializTickets;
					$response->message = 'success';
				} else {
					$response->status = 401;
					$response->message = 'Тикеты не найдены';	
				}
				break;
			}
			case 'messanger': {
				$id = isset($r['ticket-id']) ? trim($r['ticket-id']) : NULL;
				$ticket = getTicket($id, $authUser->id, $o->support);
				if (!empty($ticket)) {
					$response->status = 200;
					$response->result = $ticket;
					$response->message = 'success';

					if ($ticket->status !== 'closed') {
						$o->support[$ticket->index]->status = 'readed';

						$c = $o;
					}
				} else {
					$response->message = 'Тикет не найдена';
				}
				break;
			}
			case 'messaging': {
				$ticketID = $r['ticket-id'];
				$message = isset($r['message']) ? trim($r['message']) : NULL;

				if (empty($message)) {
					$response->status = 401;
					$response->result = NULL;
					$response->message = 'Укажите текст сообщения';
				} else {
					$ticket = getTicket($ticketID, $authUser->id, $o->support);

					if (!empty($ticket)) {
						if ($ticket->status === 'closed') {
							$response->status = 403;
							$response->message = 'Тикет закрыт и больше невозможно отправить сообщение. Если у Вас остались вопросы, Вы можете добавить новый тикет';
						} else {
							$message = createTicketmessage($message, 1);
			
							$o->support[$ticket->index]->status = 'pending';
							$o->support[$ticket->index]->content[] = $message;

							$response->status = 200;
							$response->message = 'success';
							$response->result = $o->support[$ticket->index];

							$c = $o;
						}
					} else {
						$response->message = 'Сообщение не доставлено. Пожалуйста попробуйте чуть позже';
					}
				}

				break;
			}
			case 'live-message': {
				$tickets = getTickets($authUser->id, $o->support);

				$answered = array_filter($tickets, function($ticket) {
					return $ticket->status === 'answered';
				});
				$notify = array_filter($answered, function($ticket) use ($authUser) {
					return isset($ticket->updated) ? date($authUser->lastSession) < date($ticket->updated) : false;
				});


				$messenger = NULL;
				$messangerid = isset($r['messanger']) ? trim($r['messanger']) : NULL;
				if (!empty($messangerid))
					$messenger = getTicket($messangerid, $authUser->id, $o->support);

				$response->status = 200;
				$response->result = (object)[
					"answered" => count($answered),
					"notify" => count($notify) > 0,
				];

				if (!empty($messenger)) {
					$response->result->messenger = $messenger;

					if ($messenger->status !== 'closed') {
						$o->support[$messenger->index]->status = 'readed';

						$c = $o;
					}
				} else {
					if (is_array($notify) && count($notify) > 0) {
						$response->result->updated = [];
						foreach ($notify as $index => $item)
							$response->result->updated[] = getSanitializTicket($item, $o->sections, $o->supportStatuses);
					}
				}

				$response->message = 'success';

				break;
			}
			case 'close-ticket': {
				$ticketID = $r['ticket-id'];
				$ticket = getTicket($ticketID, $authUser->id, $o->support);

				if (!empty($ticket)) {
					$o->support[$ticket->index]->status = 'closed';

					$response->status = 200;
					$response->message = 'success';
					$response->result = $o->support[$ticket->index];

					$c = $o;
				} else {
					$response->message = 'Сейчас невозможно закрыть тикет. Пожалуйста попробуйте позже';
				}

				break;
			}
			case 'generete-promocodes': {
				for ($i = 1; $i <= 10; $i++) {
					$o->promocodes[] = [
						"code" => random_code(),
						"total" => $i * 1000
					];
				}

				$c = $o;
				break;
			}
			case 'data': {
				$k = isset($r['key']) ? trim($r['key']) : NULL;

				if (!empty($k) && isset($o->$k)) {
					$response->status = 200;
					$response->message = 'OK';
					$response->result = $o->$k;
				}
				break;
			}
			case 'logout': {
				$response->status = 200;
				$response->result = true;
				$response->message = 'success';

				break;
			}
			case 'play': {

				$bet = isset($r['bet']) ? (float)$r['bet'] : NULL;
				$range = isset($r['range']) ? json_decode($r['range']) : NULL;

				if (empty($bet) || empty($range)) {
					$response->status = 403;
					$response->message = "Неправильные данные для игры. Проверьте еще раз.";
				} else {

					if ($o->users[$authUser->index]->total < $bet) {
						$response->status = 403;
						$response->message = "Не достаточно средство.";
					} else {
						$number = randmo_in_range(SLIDER__MIN, SLIDER__MAX);

						$chance = ($range[1] - $range[0]) / SLIDER__MAX * 100;
						$factor = (float)number_format(100 / $chance, 2);
						$win = (float)number_format($bet * $factor, 2);

						if ($number <= $range[1] && $number >= $range[0]) {
							$o->users[$authUser->index]->total += $win;
						} else {
							$win = 0;
							$o->users[$authUser->index]->total -= $bet;
							if ($o->users[$authUser->index]->total < 0)
								$o->users[$authUser->index]->total = 0;
						}

						$response->status = 200;
						$response->message = "success";
						$response->result = [
							"win" => $win,
							"number" => $number,
							"total" => $o->users[$authUser->index]->total
						];

						$c = $o;
					}
				}	

				break;
			}
		}

		if (!empty($authUser)) {
			$o->users[$authUser->index]->lastSession = getDateNow();

			$c = $o;
		}
	}
}

if (DEBUG) : ?>
		<main>
			<header>
				<h1>Chase-X Server</h1>

				<div>
					<a target="_blank" href="/?query=generete-promocodes&debug">Generate promocodes</a>
					<a target="_blank" href="/support.php">Support</a>
					<a href="<?= debugOfUri() ?>">
						Debug off
					</a>
				</div>
			</header>
			<section>
				<nav>
					<h2>Gid</h2>
					<ul>
						<?php foreach ($o->gid as $k => $v) : ?>
							<li>
								<a href="<?= queryUrl($v->url); ?>">
									<?= $v->label; ?>
								</a>
								<?php if (isset($v->sub)) : ?>
									<ul>
										<?php foreach ($v->sub as $kk => $vv) : ?>
											<li>
												<a href="<?= queryUrl($vv->url)?>">
													<?= $vv->label?>
												</a>
											</li>
										<?php endforeach; ?>
									</ul>
								<?php endif; ?>
							</li>
						<?php endforeach;  ?>
					</ul>
				</nav>
				<fieldset>
					<h2>Query</h2>
					<table cellpadding="0" border="0" cellspacing="0">
						<?php foreach ($r as $k => $v) : 
							if ($k === 'debug') continue; ?>
							<tr>
								<th><?= $k ?></th>
								<td><?= $v ?></td>
							</tr>
						<?php endforeach; ?>
					</table>
				</fieldset>
				<div>
					<h2>Output</h2>
					<pre>
						<code class="language-json hljs"><?=
							stripslashes(
								json_encode(
									$response,
									JSON_UNESCAPED_UNICODE |
									JSON_PRETTY_PRINT
								)
							);
						?></code>
					</pre>
				</div>
			</section>
		</main>
		<footer>
			<p>Powered by <a href="//sky-tech.org" target="_blank">SkyTech Studio</a></p>
		</footer>
		<script src="./highlight.min.js"></script>
		<script type="text/javascript">
			hljs.initHighlightingOnLoad();
		</script>
	</body>
</html>
<?php else :
	print json_encode($response, JSON_UNESCAPED_UNICODE);
endif;

if ($c !== NULL)
	saveDB($c);
