<?php

function debugOfUri() {
	if (empty($_REQUEST))
		return '/';

	$args = [];
	foreach ($_REQUEST as $k => $v) {
		if ($k === 'debug')
				continue;

		$args[] = "$k=$v";
	}

	return '/?' . implode('&', $args);
}

function getDateNow() {
	return date('Y-m-d H:i:s', strtotime("+1 hours")); 
}

function queryUrl($url) {
	if (!isset($_GET['debug']))
		return $url;

	return $url . "&debug";
}

function writeLog($message = "") {
	$date = getDateNow();
	$content = file_get_contents('log.txt');
	file_put_contents('log.txt', $content . "[$date] - $message" . "\n");
}

function getUserByToken($token, $users = []) {
	foreach ($users as $index => $user) {
		if (isset($user->token) && $user->token === $token) {
			$user->index = $index;
			
			return $user;
		}
	}

	return NULL;
}

function getPromoCode($str, $list) {
	foreach ($list as $index => $item) {
		if ($item->code === $str) {
			$item->index = $index;
			return $item;
		}
	}

	return NULL;
}

function randmo_in_range($min, $max) {
	return rand($min, $max);
}

function random_color_part() {
    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
}

function random_color() {
    return '#' . random_color_part() . random_color_part() . random_color_part();
}

function usernameExists($username, $users) {
	foreach ($users as $index => $user)
		if ($user->username === $username)
			return $user;

	return NULL;
}

function createUser($username, $password) {

	return (object)[
		"total" => 0,
		"username" => $username,
		"color" => random_color(),
		"password" => md5($password),
		"id" => bin2hex(random_bytes(5)),
	];
}

function createTicket($user, $section, $subject, $message) {
	if (empty($user))
		return null;

	$content = [];

	if (!empty($message))
		$content[] = createTicketmessage($message, 1);

	return (object)[
		"status" => 'pending',
		"section" => $section,
		"subject" => $subject,
		"content" => $content,
		"userid" => $user->id,
		"updated" => getDateNow(),
		"id" => bin2hex(random_bytes(5))
	];
}

function getSanitializTicket($tiket, $sections, $supportStatuses) {
	
	$content = null;

	if (is_array($tiket->content) && count($tiket->content) > 0) {
		foreach (array_reverse($tiket->content) as $c) {
			$content = $c;
			break;
		}
	}

	return (object)[
		"id" => $tiket->id,
		"content" => $content,
		"subject" => $tiket->subject,
		"section" => getTicketSection($tiket->section, $sections),
		"status" => getTicketStatus($tiket->status, $supportStatuses)
	];
}

function getTickets($userid, $support) {
	if (empty($userid))
		return null;

	return array_reverse(array_filter($support, function($ticket) use ($userid) {

		return $ticket->userid === $userid;
	}));
}

function getTicket($id, $userid, $support) {
	if (empty($id))
		return null;

	foreach ($support as $index => $item) {
		if ($id === $item->id && ($userid === $item->userid || $userid === 'magumba')) {
			$item->index = $index;
			return $item;
		}
	}
}

function getTicketStatus($value, $statuses = []) {

	foreach ($statuses as $key => $status)
		if ($key === $value)
				return (object)[
					"value" => $key,
					"label" => $status
				];

	return NULL;
}

function getTicketSection($value, $sections = []) {
	
	foreach ($sections as $index => $section)
		if ($value === $section->value)
			return (object)$section;

	return NULL;
}

function createTicketmessage($message, $owner) {

	return (object)[
		"owner" => $owner,
		"message" => $message,
		"date" => getDateNow()
	];
}

function recaptcha($response) {
	$post_data = http_build_query(
    array(
	    'response' => $response,
	    'secret' => CAPTCHA_SECRET,
	    'remoteip' => $_SERVER['REMOTE_ADDR']
    )
	);
	$opts = array('http' =>
	  array(
	    'method'  => 'POST',
	    'header'  => 'Content-type: application/x-www-form-urlencoded',
	    'content' => $post_data
	  )
	);
	$context  = stream_context_create($opts);
	$response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', FALSE, $context);
	$result = json_decode($response);

	return $result->success;
}

function random_code() {
	$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$res = "";
	for ($i = 0; $i < 10; $i++) {
    $res .= $chars[mt_rand(0, strlen($chars)-1)];
	}

	return $res;
}

function saveDB($data) {
	try {
		if (copy('data.json', 'backup.json')) {
			$fs = fopen('data.json', 'w');
			fwrite($fs, json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
			fclose($fs);
		} else {
			writeLog('I could not make a data backup');
		}
	} catch(Exception $error) {
		writeLog($error);
	}
}