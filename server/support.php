<?php
date_default_timezone_set('Europe/Moscow');

require_once 'fns.php';

$data = file_get_contents('data.json');
$data = json_decode(stripslashes($data));

$support = $data->support;
$lastViewDate = !empty($data->lastVeiwDate) ? $data->lastVeiwDate : getDateNow();

$ticket = NULL;
$ID = isset($_GET['id']) ? $_GET['id'] : NULL;
if (!empty($ID)) {
	$ticket = getTicket($ID, 'magumba', $support);

	if (!empty($ticket)) {
		$ticket->username = "";
		foreach ($data->users as $index => $user) {
			if ($user->id === $ticket->userid) {
				$ticket->username = $user->username;
				break;
			}
		}
	}
}

if (isset($_GET['query'])) {
	$reload = FALSE;
	$updateDB = NULL;
	$query = "?query=ticket&id={$ID}";

	switch ($_GET['query']) {
		case 'delete': {
			array_splice($data->support, $ticket->index, 1);

			$query = "";
			$reload = TRUE;
			$updateDB = $data;
			break;
		}
		case 'clear': {
			$data->support[$ticket->index]->content = [];

			$reload = TRUE;
			$updateDB = $data;

			break;
		}
		case 'block': {
			$data->support[$ticket->index]->status = 'closed';

			$data->support = (array)$data->support;

			$reload = TRUE;
			$updateDB = $data;
			break;
		}
		default: {

		}
	}

	if ($updateDB)
		saveDB($data);

	if ($reload)
		header("Location: /support.php$query");
}

if (isset($_POST['submit']) && !empty($ID)) {
	$message = isset($_POST['message']) ? trim($_POST['message']) : NULL;
	if (!empty($message)) {
		$message = [
			"owner" => 0,
			"message" => $message,
			"date" => getDateNow(),
		];

		$data->support[$ticket->index]->status = 'answered';
		$data->support[$ticket->index]->content[] = $message;
		$data->support[$ticket->index]->updated = getDateNow();

		saveDB($data);

		header("Location: /support.php?id=$ID");
	}
}

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Chase X support</title>

		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<link rel="stylesheet" href="./style.css" />
	</head>
	<body>
		<header>
			<h1>Chase-X Server</h1>
			<div>
				<a href="/?debug">API</a>
			</div>
		</header>
		<main class="support">
			<aside>
				<nav>
					<ul>
						<?php foreach ($support as $index => $item) : ?>
							<li class="<?= $item->id === $ID ? 'selected' : '' ?>">
								<a href="/support.php?query=ticket&id=<?= $item->id; ?>">
									<sup><?= getTicketSection($item->section, $data->sections)->label; ?></sup><br />
									<?= $item->subject; ?>
								</a>
							</li>
						<?php endforeach; ?>
					</ul>
				</nav>
			</aside>
			<section class="chat">
				<?php if (!empty($ticket)) : ?>
					<div class="toolbar">
						<label class="username">
							<?= $ticket->username; ?>
						</label>
						<div class="actions">
							<?php if ($ticket->status !== 'closed'): ?>
								<a href="/support.php?query=block&id=<?= $ticket->id; ?>" class="close">CLOSE</a>
							<?php endif; ?>
							<a href="/support.php?query=clear&id=<?= $ticket->id; ?>" class="back">CLEAR</a>
							<a href="/support.php?query=delete&id=<?= $ticket->id; ?>" class="back">DELETE</a>
							<a href="/support.php" class="back">BACK</a>
						</div>
					</div>

					<div class="messages">
						<?php if (is_array($ticket->content)) :
							foreach ($ticket->content as $index => $item) : ?>
								<div class="message message--<?= $item->owner; ?>">
									<span><?= $item->message ?></span>
									<time><?= $item->date ?></time>
								</div>
							<?php endforeach;							
						endif; ?>
					</div>
					<?php if ($ticket->status !== 'closed') : ?>
						<form method="POST">
							<textarea name="message" placeholder="Type text hear..."></textarea>
							<button name="submit">SEND</button>
						</form>
					<?php else : ?>
						<p style="color: var(--color-danger)">Ticket closed</p>
					<?php endif; ?>
				<?php else : ?>
					<p>No ticket selected</p>
				<?php endif; ?>
			</section>
		</main>
		<footer>
			<p>Powered by <a href="//sky-tech.org" target="_blank">SkyTech Studio</a></p>
		</footer>
	</body>
</html>