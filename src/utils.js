import Config from '@/config'

export const createQuery = (query) => {
  const formData = new FormData()

  if (localStorage.getItem(Config.LS.Token))
    formData.append('token', localStorage.getItem(Config.LS.Token))
  
  for (let key in query)
    formData.append(key, query[key])

  return formData
}

export const randomInRange = (min, max) =>
  Math.floor(Math.random() * (max - min) + min)

export const resetOverlay = () => {

  const rule = new RegExp(Config.RULES.Overlayed)
  
  document.body.classList.forEach(classname => {
    if (rule.test(classname))
      document.body.classList.remove(classname)
  })

  setTimeout(() =>
    document.body.classList.remove('overlayed'),
    250
  )
}