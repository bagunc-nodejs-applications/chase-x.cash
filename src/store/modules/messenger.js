import Vue from 'vue'

import axios from 'axios'

import Config from '@/config'
import { createQuery } from '@/utils'

export default {
  namespaced: true,
  state: {
    id: null,
    section: '',
    messages: [],
    status: 'pending',
    subject: 'Загрузка...',
  },
  mutations: {
    mergeResult(state, { id, status, subject, section, messages }) {
      if (id) state.id = id
      if (status) state.status = status
      if (subject) state.subject = subject
      if (section) state.section = section
      if (messages) state.messages = messages
    },
    pushMessage(state, messages) {
      state.messages = messages
    },
  },
  actions: {
    async getData({ commit }, query) {
      return axios.post(Config.API.URI, createQuery({
        query: 'messanger',
        'ticket-id': query.id,
      }))
        .then(({ data }) => {
          if (data.status === 200) {
            commit('mergeResult', {
              id: data.result.id,
              status: data.result.status,
              subject: data.result.subject,
              section: data.result.section,
              messages: data.result.content,
            })

            return new Promise(resolve => resolve(data))
          } else {
            if (!query.bgMode)
              Vue.systemNotify(data.status, data.message)
          }
          return new Promise(resolve => resolve(data))
        })
    },
    async messaging({ commit }, query) {
      return axios.post(Config.API.URI, createQuery({
        ...query,
        query: 'messaging',
      }))
        .then(({ data }) => {
          if (data.status === 200) {
            commit('pushMessage', data.result.content)
            return new Promise(resolve => resolve(data))
          } else {
            Vue.systemNotify(data.status, data.message)
          }
        })
    },
    async close({ commit }, id) {
      return axios.post(Config.API.URI, createQuery({
        query: 'close-ticket',
        'ticket-id': id,
      }))
        .then(({ data }) => {
          if (data.status === 200) {
            commit('mergeResult', {
              status: data.result.status
            })
            if (data.result.status === 'closed')
              commit('support/buttons', ['back'], {
                root: true
              })

            return new Promise(resolve => resolve(data.result))
          } else {
            Vue.systemNotify(data.status, data.message)
          }
        })
    }
  },
}