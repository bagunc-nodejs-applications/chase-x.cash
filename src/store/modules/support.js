import Vue from 'vue'

import axios from 'axios'

import Config from '@/config'
import { createQuery } from '@/utils'

export default {
  namespaced: true,
  state: {
    items: [],
    sections: [],
    activeButtons: [],
    heading: 'Служба поддержки',
  },
  mutations: {
    buttons(state, payload) {
      state.activeButtons = payload
    },
    sections(state, payload) {
      state.sections = payload
    },
    tickets(state, payload) {
      state.items = payload
    },
    subject(state, payload) {
      state.heading = payload 
    },
    updateItem(state, payload) {
      payload.forEach(updated => {
        state.items.forEach((item, index) => {
          if (item.id === updated.id)
            state.items.splice(index, 1, updated)
        })
      })
    },
  },
  actions: {
    async getTickets({ commit }) {
      commit('subject', 'Служба поддержки')
      return axios.post(Config.API.URI, createQuery({
        query: 'get-tickets',
      }))
        .then(({ data }) => {
          if (data.status === 200) {
            commit('tickets', data.result)
          } else {
            Vue.systemNotify(data.status, data.message)
          }
          return new Promise(resolve => resolve(data))
        })
    },
    getSections({ commit }) {
      axios.post(Config.API.URI, createQuery({
        query: 'data',
        key: 'sections',
      }))
        .then(({ data }) => {
          switch (data.status) {
            case 200:
              return commit('sections', data.result)
            default:
              return Vue.systemNotify(data.status, data.message)
          }
        })
    },
    async createTicket(state, query) {
      return axios.post(Config.API.URI, createQuery({
        ...query,
        query: 'create-ticket',
      }))
        .then(({ data }) => {
          if (data.status === 200) {
            return new Promise(resolve => resolve(data))
          } else {
            Vue.systemNotify(data.status, data.message)
          }
        })
    },
  },
}