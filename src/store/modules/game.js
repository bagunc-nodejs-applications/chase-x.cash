import Vue from 'vue'

import { SLIDER } from '@/config'
import { randomInRange } from '@/utils'

export default {
  namespaced: true,
  state: {
    bet: 1,
    win: 0,
    slider: {
      min: SLIDER.MIN,
      max: SLIDER.MAX,
    },
    shuffle: {
      value: 1.1,
      max: SLIDER.SHUFFLE.MAX,
      min: SLIDER.SHUFFLE.MIN,
      step: SLIDER.SHUFFLE.STEP,
    },
    factor: 0,
    chance: 50,
    combination: {
      value: 0,
      color: 'default',
    },
    range: [SLIDER.MIN, SLIDER.MAX],
  },
  mutations: {
    setRange(state, { min, max }) {

      min = min !== undefined ? parseInt(min) : state.range[0]
      max = max !== undefined ? parseInt(max) : state.range[1]

      if (isNaN(min))
        min = state.slider.min

      if (isNaN(max))
        max = state.slider.min

      if (min < state.slider.min)
        min = state.slider.min
      
      if (max > state.slider.max)
        max = state.slider.max

       if (min > max)
        min = max

      if (max < min)
        max = min
        

      state.range = [
        min,
        max,
      ]
    },
    setBet(state, value) {
      value = parseFloat(value)

      if (isNaN(value))
        value = 1

      state.bet = value.toFixed(2)
    },
    setRandomFactory(state, value) {
      state.shuffle.value = value
    },
    setFactor(state, value) {
      state.factor = value.toFixed(2)
    },
    setCombination(state, payload) {
      state.combination = {...state.combination, ...payload}
    }
  },
  getters: {
    getBet: state => state.bet,
    getRange: state => state.range,
    getSlider: state => state.slider,
    getCombination: state => state.combination,
    getRandomFactory: state => state.shuffle.value,
    getCount: state => state.range[1] + 1 - state.range[0],
    getWin: (state, getters) => state.bet * getters.getFactor,
    getFactor: (state, getters) => (
      100 / getters.getChance
    ).toFixed(2),
    getRangeNumbersCount: (state, getters) => {
      const chance = 100 / getters.getRandomFactory

      return Math.round(state.slider.max * chance / 100)
    },
    getChance: (state, getters) => (
      (getters.getCount / state.slider.max) * 100
    ),
  },
  actions: {
    doShuffle({ commit, state, getters }) {
      const { min, max } = state.slider

      if (!(typeof min === 'number' && typeof max === 'number'))
        return false

      const { getRangeNumbersCount } = getters;
      // if (
      //   getFactor < shuffle.min ||
      //   getFactor > shuffle.max
      // )
      //   return Vue.prototype.$notify({
      //     type: 'error',
      //     group: 'system',
      //     title: 'Ошибка перемешивания',
      //     text: `Значение поля в случайном порядке должно быть в диапазоне от ${shuffle.min} до ${shuffle.max}.`,
      //   })

      const random = randomInRange(min, max - getRangeNumbersCount)
      
      commit('setRange', {
        min: random,
        max: random + getRangeNumbersCount - 1,
      })
    },
    doBet({ commit, getters, rootState }, action) {
      let bet = getters.getBet

      const total = rootState.account.user.total
      const min = total > 0 ? 1 : 0

      const errorMsg = {
        default: 'Не достаточно средство',
        min: `Ставка не может быть меньше ${min}`,
        total: `Ставка не может быть больше ${total}`,
      }

      const error = {
        text: null,
        type: 'error',
        group: 'system',
        title: 'Ошибка ставки',
      }

      if (total) {
        switch (action) {
          case 'decrement': {
            if (--bet < min) {
              bet = min;
              error.text = errorMsg.min
            }
            break
          }
          case 'increment': {
            if (++bet > total) {
              bet = total
              error.text = errorMsg.total
            }
            break
          }
          case 'half': {
            bet *= 0.5
            if (bet < min) {
              bet = min
              error.text = errorMsg.min
            }
            break
          }
          case 'x2': {
            bet *= 2
            if (bet > total) {
              bet = total
              error.text = errorMsg.total
            }
            break
          }
          case 'min': {
            bet = min
            break
          }
          case 'total': {
            bet = total
            break
          }
        }
      } else {
        error.text = errorMsg.default
      }

      if (error.text)
        Vue.prototype.$notify(error)

      commit('setBet', bet)
    },
  },
}