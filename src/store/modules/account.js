import Vue from 'vue'
import axios from 'axios'

import Config from '@/config'
import { createQuery } from '@/utils'

import router from '@/routes/'

export default {
  namespaced: true,
  state: {
    token: localStorage.getItem(Config.LS.Token) || '',
    user: {
      id: '',
      total: 0,
      username: '',
      color: '#1190CB',
    },
    notifications: {
      support: 0,
    },
  },
  mutations: {
    setTotal(state, payload) {
      state.user.total = payload
    },
    signIn(state, { user, token }) {
      if (user)
        state.user = user
      
      if (token) {
        state.token = token
        localStorage.setItem(Config.LS.Token, token)
      }
    },
    logout(state) {
      state.user = null
      state.token = null
      localStorage.removeItem(Config.LS.Token)
    },
    supportNotify(state, value) {
      state.notifications.support = value;
    },
  },
  getters: {
    getId: state => state.user.id,
    getColor: state => state.user.color,
    getTotal: state => state.user.total,
    getUsername: state => state.user.username,

    isLoggedIn: state => state.token,
    notifySupport: (state) => state.notifications.support,
  },
  actions: {
    async signIn({ commit }, data) {
      return axios.post(Config.API.URI, createQuery({
        ...data,
        query: 'sign-in'
      }))
        .then(({ data }) => {
          if (data.status === 200) {
            const { user, token } = data.result
            commit('signIn', {
              user,
              token,
            })

          } else {
            Vue.systemNotify(data.status, data.message)
          }

          return new Promise(resolve => {
            resolve(data)
          })
        })
        .catch(error => console.error(error))
    },
    async signUp({ commit }, data) {
      return axios.post(Config.API.URI, createQuery({
        ...data,
        query: 'sign-up',
      }))
        .then(({ data }) => {
          switch (data.status) {
            case 200: {
              return new Promise(resolve => {
                commit('signIn', data.result)

                return resolve(data)
              })
            }
            default: {
              Vue.systemNotify(data.status, data.message)
            }
          }
        })
    },
    async edit({ commit }, data) {
      return axios.post(Config.API.URI, createQuery({
        ...data,
        query: 'edit-account'
      }))
        .then(({ data }) => {
          switch (data.status) {
            case 200: {
              const { user, token } = data.result
              commit('signIn', {
                user,
                token,
              })

              break
            }
            case 401: {
              commit('logout')
              Vue.notify({
                type: 'error',
                group: 'system',
                title: 'Ошибка изменения данных',
                text: 'Чтобы изменить личные данные, авторизуйтесь',
              })

              break
            }
            default: {
              Vue.systemNotify(data.status, data.message)
              break
            }
          }

          return new Promise(resolve => resolve(data))
        })
    },
    getData({ commit }, data = {}) {
      axios.post(Config.API.URI, createQuery({
        ...data,
        query: 'user',
      }))
        .then(({ data }) => {
          switch (data.status) {
            case 200: {
              this.dispatch('account/live')

              return commit('signIn', {
                user: data.result,
              })
            }
            default: {
              Vue.systemNotify(data.status, data.message)
            }
          }
          return commit('logout')
        })
        .catch(error => console.error(error))
    },
    logout({ commit }) {
      
      axios.post(Config.API.URI, createQuery({
        'query': 'logout',
      }))
      .then(({ data }) => {
          if (data.status === 200) {
            commit('logout')
            localStorage.removeItem(Config.LS.Token)
          }
        })
    },
    async live({ commit }) {

      setInterval(async () => {
        const query = {
          query: 'live-message',
        }

        if (router.currentRoute.name === "/support/messanger")
          query.messanger = router.currentRoute.params.id

        const response = await axios.post(Config.API.URI, createQuery(query))

        const { data } = response;

        if (data.status === 200) {
          const { notify, answered, updated, messenger } = data.result

          if (notify)
            Vue.prototype.$audio.notify.play()

          if (updated)
            commit('support/updateItem', updated, {
              root: true
            })

          if (messenger)
            commit('messenger/pushMessage', messenger.content, {
              root: true
            })

          commit('supportNotify', answered)
        }
      }, Config.LIVE.Timeout)

    },
  },
}