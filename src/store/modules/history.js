export default {
  state: {
    count: 20,
    topCount: 10,
    list: [
      {
        id: 1,
        chance: 100,
        number: 4658,
        amount: 1000,
        winnings: 1010,
        range: [0, 5000000],
        username: 'Happy Player',
      }
    ],
    top: [],
  },
  mutations: {
    pushItem(state, item) {
      if (state.list.length >= state.count)
        state.list.pop()
      
      state.list = [...state.list, item]
    },
    pushTop(state, item) {
      if (state.top.length >= state.count)
        state.top.pop()
      
      state.top = [...state.top, item]
    }
  }
}