import Vue from 'vue'
import Vuex from 'vuex'

import axios from 'axios'

import Config from '@/config'
import { createQuery } from '@/utils'

import game from '@/store/modules/game'
import history from '@/store/modules/history'
import account from '@/store/modules/account'
import support from '@/store/modules/support'
import messenger from '@/store/modules/messenger'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  state: {
    menu: {
      header: {
        loading: true,
        list: [
          { label: 'Loading', url: '/loading-1', key: 'loading__1' },
          { label: 'Loading', url: '/loading-2', key: 'loading__2' },
          { label: 'Loading', url: '/loading-3', key: 'loading__3' },
        ],
      },
      sidebar: {
        loading: true,
        list: [
          { url: "/loading-1", key: "loading__1", label: "Loading", },
          { url: "/loading-2", key: "loading__2", label: "Loading", },
          { url: "/loading-3", key: "loading__3", label: "Loading", },
        ]
      },
    },
  },
  mutations: {
    setMenus({ menu }, { header, sidebar }) {
      if (header) {
        menu.header.list = header
        menu.header.loading = false
      }
      
      if (sidebar) {
        menu.sidebar.list = sidebar
        menu.sidebar.loading = false
      }
    },
    setMenu(state, payload) {
      state.menu = {...state.menu , [payload.location]: payload.result}
    },
  },
  actions: {
    async getMenus({ commit }) {
      return axios.post(Config.API.URI, createQuery({
                query: 'menu',
              }))
              .then(({ data }) => {
                
                if (data.status === 200)
                  commit('setMenus', data.result)
              })
    },
    async getMenu({ commit }, location) {
      return axios.post(Config.API.URI, createQuery({
                query: 'menu',
                location: location,
              }))
                .then(({ data }) => 
                  data.status === 200 ?
                    commit('setMenu', {
                      location,
                      result: {
                        loading: false,
                        list: data.result,
                      },
                    }) :
                      Vue.notify({
                        type: 'error',
                        group: 'system',
                        text: data.message,
                        title: `Системная ошибка с кодом ${data.status}`,
                      }))
    }
  },
  modules: {
    game,
    history,
    account,
    support,
    messenger,
  },
  strict: debug,
})