
export const notify = new Audio('/assets/sounds/notify.mp3')

export default {
  install(Vue) {
    Vue.prototype.$audio = {
      notify,
    }
  }
}