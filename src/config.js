export const API = {
  URI: process.env.NODE_ENV === 'production' ? 'http://test.sky-tech.org/' : 'http://localhost:80/',
}

export const LS = {
  Token: 'c-token',
}

export const RULES = {
  Overlayed: /overlayed--(?=.*[a-z])+/g,
  Password: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/,
}

const LIVE = {
  Timeout: 3000
}

export const SLIDER = {
  MIN: 1,
  MAX: 5000,
  SHUFFLE: {
    MIN: 1.1,
    STEP: 0.1,
    MAX: 5000,
  },
}

export default {
  LS,
  API,
  LIVE,
  RULES,
  SLIDER,
}