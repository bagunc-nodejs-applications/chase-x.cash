import Vue from 'vue'

import axios from 'axios'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import VueTimeago from 'vue-timeago'
import Notifications from 'vue-notification'
import VueNumber from 'vue-number-animation'
import VueContentPlaceholders from 'vue-content-placeholders'

import App from './App.vue'
import store from './store'
import router from './routes'
import audio from './plugins/audio'
 
Vue.config.productionTip = false

Vue.use(audio)
Vue.use(VueNumber)
Vue.use(VueRouter)
Vue.use(Notifications)
Vue.use(VueAxios, axios)
Vue.use(VueContentPlaceholders)
Vue.use(VueTimeago, {
  locale: 'ru',
  name: 'Timeago',
  locales: {
    'ru': require('date-fns/locale/ru'),
  },
})

Vue.systemNotify = (status, text) => {
  Vue.notify({
    text,
    type: 'error',
    group: 'system',
    title: `Системная ошибка с кодом ${status || 500}`,
  })
}

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')