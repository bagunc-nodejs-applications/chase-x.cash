import Vue from 'vue'
import Router from 'vue-router'

import FAQ from '@/routes/FAQ'
import Page from '@/routes/Page'
import Settings from '@/routes/Settings'
import Contacts from '@/routes/Contacts'
import PromoCode from '@/routes/PromoCode'

import Home from '@/routes/Home'
import Login from '@/components/c-form-login'
import Registration from '@/components/c-form-registration'

import Support from '@/routes/Support'
import Messanger from '@/components/c-messanger'
import NewTicket from '@/components/c-new-ticket'
import AllTickets from '@/components/c-all-tickets'

import { resetOverlay } from '@/utils'

import store from '../store'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      name: 'faq',
      path: '/faq',
      component: FAQ,
    },
    {
      name: 'settings',
      path: '/settings',
      component: Settings,
      meta: {
        requiresAuth: true,
      },
    },
    {
      name: 'page',
      component: Page,
      path: '/page/:slug',
    },
    {
      name: 'contacts',
      path: '/contacts',
      component: Contacts,
    },
    {
      name: 'promocode',
      path: '/promo-code',
      component: PromoCode,
      meta: {
        requiresAuth: true,
      },
    },
    {
      name: 'logout',
      path: '/logout',
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/support/:screen?',
      component: Support,
      meta: {
        requiresAuth: true,
      },
      children: [
        {
          component: Messanger,
          name: '/support/messanger',
          path: '/support/messanger/:id',
        },
        {
          name: 'support-new',
          path: '/support/new',
          component: NewTicket,
        },
        {
          path: '',
          name: 'support-all',
          component: AllTickets,
        },
      ],
    },
    {
      path: '/:screen?',
      component: Home,
      children: [
        {
          name: 'registration',
          path: '/registration',
          component: Registration,
        },
        {
          path: '',
          name: 'home',
          component: Login,
        },
      ],
    },
  ],
  scrollBehavior: ({ name, params }) => {
    
    if (name === 'home')
      if (['all', 'my'].includes(params.screen))
        return;

    return ({
      x: 0,
      y: 0,
    })
  }
})

router.beforeEach((to, from, next) => {
  if (to.name === 'logout') {
    store.dispatch('account/logout')

    next('/')
  }

  if (to.matched.some(record => record.meta.requiresAuth))
    if (store.getters['account/isLoggedIn'])
      next()
    else
      next('/')
  else
    next()

  resetOverlay()
})

export default router